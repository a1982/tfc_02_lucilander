package com.example.tfc_02

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.text.SimpleDateFormat
import java.util.*

const val USER_ID = 0
const val OTHER_ID = 1
class MainActivity : AppCompatActivity() {

    private var fromUser = true

    private lateinit var rvwList: RecyclerView
    private lateinit var etxtMessage: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.rvwList = findViewById(R.id.rvwList)
        this.etxtMessage = findViewById(R.id.etxtMessage)

        //configurando RecyclerView
        this.rvwList.layoutManager = LinearLayoutManager(this)
        rvwList.adapter = MessageAdapter()

    }

    fun onClickSend(v: View){
        var messageText = this.etxtMessage.text.toString()
        if(messageText.trim().isNotEmpty()) {
            //this.txtMessage.text = message
            this.etxtMessage.setText("")

            val adapter = rvwList.adapter
            if(adapter is MessageAdapter){
                val message = ChatMessage(messageText, if(fromUser) USER_ID else OTHER_ID)
                adapter.addItem(message)
                rvwList.scrollToPosition(adapter.itemCount-1)
                fromUser = !fromUser
            }
        }
    }
}